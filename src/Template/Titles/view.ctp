<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Title $title
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar título'), ['action' => 'edit', $title->emp_no, $title->title, $title->from_date->format('Y-m-d')]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar título'), ['action' => 'delete', $title->emp_no, $title->title, $title->from_date->format('Y-m-d')], [
            'confirm' => __('¿Estás seguro de eliminar el registro?')
        ]) ?> </li>
        <li><?= $this->Html->link(__('Lista de títulos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo título'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="titles view large-9 medium-8 columns content">
    <h3><?= h($title->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Título') ?></th>
            <td><?= h($title->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('No. Empleado') ?></th>
            <td><?= $this->Number->format($title->emp_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha inicio') ?></th>
            <td><?= h($title->from_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha fin') ?></th>
            <td><?= h($title->to_date) ?></td>
        </tr>
    </table>
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Title[]|\Cake\Collection\CollectionInterface $titles
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Nuevo título'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="titles index large-9 medium-8 columns content">
    <h3><?= __('Títulos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('emp_no', 'No. Empleado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title', 'Título') ?></th>
                <th scope="col"><?= $this->Paginator->sort('from_date', 'Fecha inicio') ?></th>
                <th scope="col"><?= $this->Paginator->sort('to_date', 'Fecha fin') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($titles as $title): ?>
            <tr>
                <td><?= $this->Number->format($title->emp_no) ?></td>
                <td><?= h($title->title) ?></td>
                <td><?= h($title->from_date) ?></td>
                <td><?= h($title->to_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $title->emp_no, $title->title, $title->from_date->format('Y-m-d')]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $title->emp_no, $title->title, $title->from_date->format('Y-m-d')]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), [
                        'action' => 'delete', $title->emp_no, $title->title, $title->from_date->format('Y-m-d')], 
                        ['confirm' => __('¿Estás seguro de eliminar el registro?')]
                    ) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    
    <?php echo $this->element('Paginador'); ?>
</div>
